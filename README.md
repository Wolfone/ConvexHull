# Convex Hull Calculator
## Allgemeine Anmerkungen
Der implementierte Algorithmus macht gewisse Vereinfachungen um etwas übersichtlicher zu werden, da er per
se bereits recht umfangreich ist:

* koplanare Flächen sind erlaubt
* erstellte Polytope sind vollständig trianguliert.

Daraus resultiert insbesondere, dass die ausgegebene Menge an Punkten zur Beschreibung des Randes einer konvexen Hülle
**nicht zwangsweise minimal** ist. Dazu wären zusätzliche Bereinigungsschritte nötig, sowie eine Erweiterung der
Datenstruktur für Oberflächen (siehe ./[..]/Face.java), die im aktuellen Zustand lediglich Dreiecke im Raum beschreibt.

Um das anschaulicher nachvollziehbar zu machen lohnt es sich die Option zu nutzen errechnete konvexe Hüllen in
eine obj-Datei exportieren zu lassen und einmal zu betrachten. Siehe dazu den späteren Abschnitt *Ausführung*.

 
## Setup
Um das nötige Kompilat zu erhalten existieren zwei Wege:

### Per executable Jar
Es existiert ein bereits kompiliertes executable jar innerhalb des Repositories, das nach Download sofort einsatzbereit ist:

    ./target/convexHull.jar

### Build from source
Falls es gewünscht sein sollte das Kompilat selbst zu erstellen, werden benötigt:
* Java Development Kit (V8+)
* Maven

Das Projekt liegt als Maven-Projekt vor; es kann von:

    https://gitlab.com/Wolfone/ConvexHull

als zip/tar/... geladen und entpackt werden. Falls git installiert ist,
bietet sich ein git-clone an:

    git clone https://gitlab.com/Wolfone/ConvexHull

Von der project-root kann dann mittels:

    mvn clean compile assembly:single

das Kompilat sowie ein executable jar in *./target* erstellt werden.
Unter Umständen ist zuvor noch ein:

    mvn generate-resources install

nötig.

## Ausführung
Für die Ausführung wird benötigt:
* Java 8+

*Angegebene Pfade sind relativ zu dem Ordner anzugeben, von dem der Aufruf des
Programms ausgeht!*
Die zu verarbeitenden Daten müssen als *csv*-Datei folgender Form vorliegen oder per Flag bei Aufruf
randomisiert erzeugt werden (siehe optionale Parameter weiter unten):

    Wert11, Wert12, Wert13
    Wert21, Wert22, Wert23
    ...

Beispieldatensätze sind in der project-root vorhanden (alle Dateien mit Endung csv).

Ist das executable jar vorhanden, hat ein Aufruf des Programms per CLI folgende Struktur:

    java -jar {PFAD/ZU/EXECUTABLE/JAR/}convexHull.jar {PFAD/ZU/CSV/DATEINAME}.csv {OPTIONALE_PARAMTER}

Dabei stehen folgende optionale Parameter zur Verfügung:

* **-v** bzw. **--verboseLogging**: erzeugt einen umfangreicheren logging-output der   
  Operationen zur Laufzeit bzw. eine umfangreichere textuelle Repräsentation des erzeugten Outputs
  mit vollständiger Beschreibung sämtlicher Faces. Für ausreichend große Punktmengen kann daher
  der Output leicht die default-Werte der anzeigbaren Terminal-Zeilen einschlägiger Betriebssysteme und IDEs 
  überschreiten, sodass sich diese Option hauptsächlich für kleinere Beispiele eignet.
* **-s** bzw. **--safeMode**: bewirkt umfangreichere Tests der Integrität der genutzten
  Listen; dieser Parameter hat lediglich debugging-Funktion und sollte i.d.R.
  nicht gesetzt werden, da er bewirkt, dass die Kosten bestimmter delete-Operationen
  in ihrere Komplexität von O(1) auf O(n) ansteigen.
* **-r** bzw. **--randomized**: fragt nach Parametern, anhand deren ein randomisierter Input erzeugt wird
  , sollte dennoch ein Input-File gegeben worden sein, wird dieses ignoriert
* **-e** bzw. **--export**: erstellt einen Export der berechneten Hüllenrepräsentation im obj-Format.
  Dieses kann zum Beispiel mit der Open-Source-Software Blender importiert und betrachtet werden.
  Hier empfiehlt es sich insbesondere das importierte Objekt nach Auswahl (Rechtsklick) im Edit-Modus
  zu betrachten (Tabulator-Taste), da so sämtliche Kanten und Knoten sichtbar werden; sprich auch solche
  Knoten, die eine Unterteilung einer Kante bewirken und somit im flächig gerenderten Objekt unsichtbar würden.
* **-c** bzw. **--configurationFile**: nur möglich, wenn weder -v noch -s
  genutzt werden; ermöglicht eine Konfiguration des Programms mittels externer Datei
  mit Namen configuration.properties, die sich am Aufrufort des Programms befinden muss.
  In ihr können diese Parameter als eine Liste von Key=Value - Paaren spezifiziert
  werden. (Diese Option existiert hauptsächlich, falls für mögliche zukünftige
  Erweiterungen des Programms einmal weitere Konfigurationsmöglichkeiten hinzukommen,
  um diese wiederverwendbarer zu gestalten. Aktuell kann diese Option vernachlässigt
  werden; ein valides Beispiel findet sich dennoch in *./configuration.properties*)


### Anmerkungen zum Export
Eine durch die Export-Funktion erstellte Datei kann zum Beispiel mit der Open-Source-Software Blender importiert 
und betrachtet werden. (Es existieren leichtgewichtigere Alternativen, allerdings meist mit geringerem Funktionsumfang oder kostenpflichtig.) 
  
https://www.blender.org/
    
Hier empfiehlt es sich insbesondere das importierte Objekt nach dessen Auswahl (Rechtsklick) im Edit-Modus
zu betrachten (Tabulator-Taste), da so sämtliche Kanten und Knoten sichtbar werden; das heißt insbesondere auch solche
Knoten, die eine Unterteilung einer Kante bewirken und somit im flächig gerenderten Objekt unsichtbar würden.
Rotation geschieht per default durch Drücken der mittlere Maustaste.
