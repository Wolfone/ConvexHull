package bous.algorithms.researchPractice.utility.math;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimitiveChecksTest {

    @Test
    void calculateDeterminant_shouldReturnZero() {
        int[] a = new int[]{0,1,0};
        int[] b = new int[]{0,2,0};
        int[] c = new int[]{0,1,2};

        assertEquals(0, PrimitiveChecks.calculateSimpleDeterminant(a,b,c));
    }

    @Test
    void calculateDeterminant_shouldReturnFour() {
        int[] a = new int[]{1,1,0};
        int[] b = new int[]{0,2,0};
        int[] c = new int[]{0,1,2};

        assertEquals(4, PrimitiveChecks.calculateSimpleDeterminant(a,b,c));
    }
}