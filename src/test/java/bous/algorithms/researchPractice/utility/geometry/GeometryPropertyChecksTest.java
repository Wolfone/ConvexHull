package bous.algorithms.researchPractice.utility.geometry;

import bous.algorithms.researchPractice.geometry.structures.Face;
import bous.algorithms.researchPractice.geometry.structures.Vertex;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static bous.algorithms.researchPractice.utility.geometry.GeometryPropertyChecks.*;

class GeometryPropertyChecksTest {

    @Test
    void checkVertexCollinearity_shouldReturnFalse_forNonCollinearVertices() {
        Vertex a = new Vertex(new int[]{1,1,0});
        Vertex b = new Vertex(new int[]{0,2,0});
        Vertex c = new Vertex(new int[]{0,1,2});

        assertEquals(false, checkVertexCollinearity(a,b,c));
    }

    @Test
    void checkVertexCollinearity_shouldReturnTrue_forCollinearVertices() {
        Vertex a = new Vertex(new int[]{0,1,0});
        Vertex b = new Vertex(new int[]{0,2,0});
        Vertex c = new Vertex(new int[]{0,1,2});

        assertEquals(true, checkVertexCollinearity(a,b,c));
    }

    @Test
    void calculateVolumeSign_shouldReturn1(){
        Vertex vertex = new Vertex(new int[]{1,1,1});
        Face face = new Face(new Vertex(0,0,0), new Vertex(0,1,0), new Vertex(1,0,0));

        assertEquals(1, calculateVolumeSign(face, vertex));
    }

    @Test
    void calculateVolumeSign_shouldReturnMinus1(){
        Vertex vertex = new Vertex(new int[]{10,10,10});
        Face face = new Face(new Vertex(0,0,10), new Vertex(10,10,0), new Vertex(0,10,10));

        assertEquals(-1, calculateVolumeSign(face, vertex));
    }

    @Test
    void calculateVolumeSign_shouldReturnZero(){
        Vertex vertex = new Vertex(new int[]{1,1,0});
        Face face = new Face(new Vertex(0,0,0), new Vertex(0,1,0), new Vertex(1,0,0));

        assertEquals(0, calculateVolumeSign(face, vertex));
    }
}