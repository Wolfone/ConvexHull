package bous.algorithms.researchPractice.utility.datastructures;

import bous.algorithms.researchPractice.geometry.structures.Vertex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import bous.algorithms.researchPractice.utility.datastructures.CircularList.Node;

import static org.junit.jupiter.api.Assertions.*;

class CircularListTest {
    Vertex a, b, c;
    CircularList<Vertex> circularList;

    @BeforeEach
    public void init(){
        circularList = new CircularList<>();
        a = new Vertex(new int[]{1, 2, 3});
        b = new Vertex(new int[]{1, 2, 1});
        c = new Vertex(new int[]{1, 2, 0});
    }

    @Test
    void append_shouldCreateListOfLengthOne_whenUsedOnEmptyListWithNonNullInput() {
        circularList.append(a);

        assertEquals(1, circularList.getLength());
    }

    @Test
    void append_shouldSetNewElementsNextToFirst() {
        circularList.append(a);
        circularList.append(b);

        assertEquals(circularList.findNodeByValue(a), circularList.findNodeByValue(b).getNext());
    }

    @Test
    void makeHead_shouldSetInputToBeNewHead_onNonEmptyListInSafeMode(){
        CircularList<Vertex> circularListSafe = new CircularList<>(true);
        circularListSafe.append(a);
        circularListSafe.append(b);
        circularListSafe.append(c);

        Node<Vertex> nb = circularListSafe.findNodeByValue(b);

        circularListSafe.makeHead(nb);

        assertEquals(circularListSafe.getFirst(), nb.getHead());
    }

    @Test
    void makeHead_shouldUpdateOtherNodesHead_onNonEmptyListInSafeMode(){
        CircularList<Vertex> circularListSafe = new CircularList<>(true);
        circularListSafe.append(a);
        circularListSafe.append(b);
        circularListSafe.append(c);

        Node<Vertex> nb = circularListSafe.findNodeByValue(b);
        Node<Vertex> na = circularListSafe.findNodeByValue(a);

        circularListSafe.makeHead(nb);

        assertEquals(circularListSafe.getFirst(), na.getHead());
    }

    @Test
    void makeHead_firstShouldBeNewHead_onNonEmptyList(){
        circularList.append(a);
        circularList.append(b);

        circularList.makeHead(circularList.findNodeByValue(b));

        assertEquals(circularList.getFirst(), circularList.findNodeByValue(b));
    }

    @Test
    void delete_shouldReturnNull_whenUsedOnEmptyList() {
        assertNull(circularList.delete(a));
    }

    @Test
    void delete_shouldUpdateHead_ifFirstElementIsDeletedInSafeMode() {
        CircularList<Vertex> circularListSafe = new CircularList<>(true);
        circularListSafe.append(a);
        circularListSafe.append(b);
        circularListSafe.delete(a);

        assertEquals(circularListSafe.findNodeByValue(b), circularListSafe.findNodeByValue(b).getHead());
    }

    @Test
    void findNodeByValue_shouldReturnNodeWithCorrectValue() {
        circularList.append(a);
        circularList.append(b);

        Vertex result = circularList.findNodeByValue(b).getValue();

        assertEquals(b, result);
    }
}