package bous.algorithms.researchPractice.utility.geometry;

import bous.algorithms.researchPractice.geometry.structures.Vertex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.jetbrains.annotations.NotNull;
import bous.algorithms.researchPractice.utility.datastructures.CircularList;

/**
 * Helper-Class to provide methods for reading geometry.
 */
public class GeometryReader {
    private static Logger logger = LogManager.getLogger(GeometryReader.class);

    /**
     * Reads vertices from a CSV-file; see the Repo-Readme for more information.
     * @param file File from which should be read.
     * @param verboseLogging Turns verbose logging on/off.
     * @param safeMode Specifies if the used lists should operate in safe-mode; debugging purposes only.
     * @see <a href="https://gitlab.com/Wolfone/ConvexHull">Source-Code-Repository</a>
     * @return A circular list made from the read vertices.
     */
    public static CircularList<Vertex> readVerticesFromCSV(@NotNull File file, boolean verboseLogging, boolean safeMode){
        String line;
        CircularList<Vertex> vertices = new CircularList<>(safeMode);
        String[] lineTokens;
        Vertex tempVertex;
        int lineNumber = 0;

        try(FileReader fileReader = new FileReader(file); BufferedReader bufferedReader = new BufferedReader(fileReader)){

            logger.info("Begin reading vertices from file: " + file.getAbsolutePath());

            while((line = bufferedReader.readLine()) != null){
                int[] parsedNumbers = new int[3];
                lineNumber++;
                lineTokens = line.split(",[ ]*");

                if(lineTokens.length != 3){
                    logger.error("Line " + lineNumber + " does not have the expected length of 3!");
                    throw new IllegalArgumentException("Line " + lineNumber + " does not have the expected length of 3!");
                }

                for (int i = 0; i < 3; i++){
                    parsedNumbers[i] = Integer.parseInt(lineTokens[i]);
                }

                tempVertex = new Vertex(parsedNumbers);
                tempVertex.setId(lineNumber - 1);

                if(verboseLogging){
                    logger.info("Created vertex: " + tempVertex);
                }

                vertices.append(tempVertex);
            }

            logger.info("Finished reading vertices from file.");

        }catch (NumberFormatException exception){
            exception.printStackTrace();
            logger.error("One of the tokens in line " + lineNumber + " seems not to be an integer!");
        }catch (IllegalArgumentException exception){
            exception.printStackTrace();
        }catch (IOException exception){
            logger.error("A problem occured while trying to read the file with path: " + file.getPath());
            exception.printStackTrace();
        }

        return vertices;
    }
}
