package bous.algorithms.researchPractice.utility.geometry;

import bous.algorithms.researchPractice.geometry.structures.Face;
import bous.algorithms.researchPractice.geometry.structures.Vertex;
import bous.algorithms.researchPractice.utility.math.PrimitiveChecks;

/**
 * Helper-Class to provide check-methods for geometric properties.
 */
public class GeometryPropertyChecks {

    /**
     * Checks three given vertices for collinearity.
     * @param vertexA the first vertex
     * @param vertexB the second vertex
     * @param vertexC the third vertex
     * @return true if the vertices are collinear, false otherwise
     */
    public static boolean checkVertexCollinearity(Vertex vertexA, Vertex vertexB, Vertex vertexC){

        int[] a = vertexA.getVertexCoordinates();
        int[] b = vertexB.getVertexCoordinates();
        int[] c = vertexC.getVertexCoordinates();

        int determinant = PrimitiveChecks.calculateSimpleDeterminant(a,b,c);

        if (determinant == 0){
            return true;
        }else {
            return false;
        }

    }

    /**
     * Calculates the sign of the volume for a given face and an additional vertex with respect to the
     * faces orientation (which is implicitly given through the order of its defining vertices).
     * @param face the given face
     * @param vertex the given vertex
     * @return 1 for a positive, -1 for a negative and 0 for a zero-volume
     */
    public static int calculateVolumeSign(Face face, Vertex vertex){

        double volume;

        double ax, ay, az;
        double bx, by, bz;
        double cx, cy, cz;

        Vertex[] faceVertices = face.getDefiningVertices();

        ax = faceVertices[0].getX() - vertex.getX();
        ay = faceVertices[0].getY() - vertex.getY();
        az = faceVertices[0].getZ() - vertex.getZ();

        bx = faceVertices[1].getX() - vertex.getX();
        by = faceVertices[1].getY() - vertex.getY();
        bz = faceVertices[1].getZ() - vertex.getZ();

        cx = faceVertices[2].getX() - vertex.getX();
        cy = faceVertices[2].getY() - vertex.getY();
        cz = faceVertices[2].getZ() - vertex.getZ();

        volume =  ax * (by*cz - bz*cy)
                + ay * (bz*cx - bx*cz)
                + az * (bx*cy - by*cx);

        if (volume > 0.5){
            return 1;
        }else if (volume < -0.5){
            return -1;
        }else{
            return 0;
        }

    }

}
