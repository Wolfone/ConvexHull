package bous.algorithms.researchPractice.utility.geometry;

import bous.algorithms.researchPractice.geometry.structures.Vertex;
import bous.algorithms.researchPractice.utility.datastructures.CircularList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;

/**
 * Helper-Class for Geometry-Instantiation.
 */
public class GeometryFactory {
    private static Logger logger = LogManager.getLogger(GeometryReader.class);

    /**
     * Randomly generates a set of positive-value distinct vertices based on a given integer max-value for a single coordinate and
     * a total number of vertices to generate. Note that if the number of vertices is close to the maximum number
     * possible with the given max value the real runtime could become quite big. If a "maximum cube" should be constructed
     * just enter the edge-length of the cube as max value and a total number that is bigger than (maxValue+1)^3.
     * @param maxValue maximum value for a single coordinate of the produced vertices
     * @param totalNumber the total number of vertices to be produced
     * @return a bous.algorithms.researchPractice.utility.datastructures.CirculaList containing the produced vertices
     */
    public static CircularList<Vertex> randomizedVertexList(int maxValue, int totalNumber){
        CircularList<Vertex> vertices = new CircularList<>();

        Vertex tempVertex;
        int id = 0;

        if (totalNumber >= Math.pow(maxValue + 1,3)){
            logger.info("The entered number of vertices to be generated exceeded the possible number of distinct vertices" +
                    " for the given max-value. The maximum possible number of vertices is generated.");
            for (int i = 0; i < maxValue+1; i++) {
                for (int j = 0; j < maxValue+1; j++) {
                    for (int k = 0; k < maxValue+1; k++) {
                       vertices.append(new Vertex(i,j,k,id++));
                    }
                }
            }
        }else{
            Random random = new Random();
            CircularList.Node<Vertex> currentVertex;
            boolean foundEqualVertex;
            while (id < totalNumber){

                tempVertex = new Vertex(random.nextInt(maxValue+1), random.nextInt(maxValue+1), random.nextInt(maxValue+1));
                currentVertex = vertices.getFirst();

                if (currentVertex != null){

                    do {
                        foundEqualVertex = tempVertex.hasSameCoordinates(currentVertex.getValue());
                    }while(currentVertex != vertices.getFirst() && !foundEqualVertex);

                    if (!foundEqualVertex){
                        tempVertex.setId(id++);
                        vertices.append(tempVertex);
                    }

                }else{
                    tempVertex.setId(id++);
                    vertices.append(tempVertex);
                }
            }
        }
        return vertices;
    }
}
