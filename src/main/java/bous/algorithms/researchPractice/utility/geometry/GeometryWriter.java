package bous.algorithms.researchPractice.utility.geometry;

import bous.algorithms.researchPractice.geometry.ConvexHullCalculator;
import bous.algorithms.researchPractice.geometry.structures.Face;
import bous.algorithms.researchPractice.geometry.structures.Polytope;
import bous.algorithms.researchPractice.geometry.structures.Vertex;
import bous.algorithms.researchPractice.utility.datastructures.CircularList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * Helper-Class to write geometry.
 */
public class GeometryWriter {
    private static Logger logger = LogManager.getLogger(GeometryReader.class);

    /**
     * Parses a given polytope to comply with obj-format and writes it to the specified path.
     * @param path Destination where the export should be saved to.
     * @param polytope The polytope to be parsed.
     */
    public static void exportPolytopeToObjFile(String path, Polytope polytope){
        //prepare polytope for parsing
        polytope.reindexPolytope();
        File file = new File(path);

        if (!file.getPath().endsWith(".obj")){
            logger.error("The specified file-ending indicates usage of an unsupported file-format. No file will be written.");
            System.exit(0);
        }else{
            try (FileWriter fileWriter = new FileWriter(file); BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){
                logger.info("Write to file " + file.getAbsolutePath() + ": ");

                StringBuilder builder = new StringBuilder();
                builder.append("mtllib ./vp.mtl\n\n");
                builder.append("g\n");

                CircularList<Vertex> vertices = polytope.getVertices();
                Iterator<Vertex> vertexIterator = vertices.iterator();
                while (vertexIterator.hasNext()) {
                    Vertex currentVertex =  vertexIterator.next();
                    builder.append("v " + currentVertex.getX() + " "
                                    + currentVertex.getY() + " "
                                    + currentVertex.getZ() + "\n");
                }
                builder.append("# " + vertices.getLength() + " vertices\n\n");
                builder.append("# 0 vertex parms\n\n");
                builder.append("# 0 texture vertices\n\n");
                builder.append("# 0 normals\n\n");
                builder.append("g polytope\n");
                builder.append("usemtl white\n");

                CircularList<Face> faces = polytope.getFaces();
                Iterator<Face> faceIterator = faces.iterator();
                while (faceIterator.hasNext()) {
                    Face currentFace =  faceIterator.next();
                    builder.append("f " + currentFace.getDefiningVertices()[0].getId() + " "
                            + currentFace.getDefiningVertices()[1].getId() + " "
                            + currentFace.getDefiningVertices()[2].getId() + "\n");
                }
                builder.append("# " + faces.getLength() + " elements");
                bufferedWriter.write(builder.toString());
                logger.info("Writing-process finished.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
