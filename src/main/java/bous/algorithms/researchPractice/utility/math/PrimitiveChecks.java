package bous.algorithms.researchPractice.utility.math;

/**
 * Helper-Class to provide primitive mathematical checks.
 */
public class PrimitiveChecks {

    /**
     * Fast calculation for a determinant of a 3x3 Matrix with integer values.
     * @param a First column of the matrix.
     * @param b Second column of the matrix.
     * @param c Third column of the matrix.
     * @return The determinant-value.
     */
    public static int calculateSimpleDeterminant(int[] a, int[] b, int[] c){
        if(a.length != 3 || b.length != 3 || c.length != 3){
            throw new IllegalArgumentException("One of the input arrays has not a length of 3!");
        }
        int determinant = a[0]*b[1]*c[2]
                - a[0]*b[2]*c[1]
                - b[0]*a[1]*c[2]
                + b[0]*a[2]*c[1]
                + c[0]*a[1]*b[2]
                - c[0]*a[2]*b[1];
        return determinant;
    }

}
