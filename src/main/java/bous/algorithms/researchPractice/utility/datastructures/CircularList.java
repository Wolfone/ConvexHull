package bous.algorithms.researchPractice.utility.datastructures;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import bous.algorithms.researchPractice.utility.geometry.GeometryReader;
import org.jetbrains.annotations.NotNull;

import java.util.Iterator;


/**
 * Implements a generic circular list. Beware that delete-operations on first elements in such a list will be O(n) if safe-mode
 * is enabled, otherwise they will be O(1). So safe mode should only be used for debugging purposes.
 * @param <T> Type of the objects that should be safed as values inside the list-nodes.
 */
public class CircularList<T> implements Iterable{
    private static Logger logger = LogManager.getLogger(GeometryReader.class);

    /**
     * First element of the list, null if the list is empty.
     */
    private Node<T> first;

    /**
     * Length of the list. 0 if the list is empty.
     */
    private int length;

    /**
     * If safeMode is FALSE the head-field of nodes is NOT used! No checks are performed if nodes are part of
     * the correct list. This fastens for example some delete operations but gives more room for errors.
     */
    private boolean safeMode;

    /**
     * Generates an empty list (length 0).
     */
    public CircularList(){
        this.first = null;
        this.length = 0;
        this.safeMode = false;
    }
    /**
     * Generates an empty list (length 0) with safeMode enabled.
     */
    public CircularList(boolean safeMode){
        this.first = null;
        this.length = 0;
        this.safeMode = safeMode;
    }

    /**
     * Appends a node with specified value to the list.
     * @param value value for node to be added
     * @return the newly added node
     */
    public Node<T> append(T value){
        Node<T> node = new Node<>(value);

        if (first != null){
            node.next = first;
            node.previous = first.previous;
            first.previous.next = node;
            first.previous = node;
        }else {
            this.first = node;
            first.previous = node;
            first.next = node;
        }

        if ( safeMode ){
            node.head = first;
        }

        this.length++;

        return node;
    }


    /**
     * Deletes the first occurence of a node with specified value. Note, that deleting by value is more costly
     * than deleting by "known node" for a linear search has to be done beforehand.
     * @param value value to delete
     * @return the deleted node
     */
    public Node<T> delete(T value){
        return delete(findNodeByValue(value));
    }

    /**
     * Deletes node from this list if the node belongs to this list. Note that in safe-mode this method is O(1) for
     * most operations except the deletion of the first list element which is O(n) where n = "list-length". This is due
     * to the necessity to update the head-references for all remaining elements when deleting the first element.
     * If safe mode is disabled the head-field of nodes HAS NO MEANING and any deletion will be performed in O(1)
     * without checks if the node belongs to the correct list.
     * Further note, that for the sake of flexibility, the next and previous references of deleted nodes will NOT
     * be cleared, meaning: they will not be accessible from inside the list BUT they can be used as a doorway to
     * reach inside the list even if they are not any longer part of it.
     * @param node node to be deleted
     * @return the deleted node, null if node not existent or the node didn't belong to this list or input was null
     */
    public Node<T> delete(Node<T> node){
        //delete can only be done properly if list is not empty and input was not null
        if (this.length == 0){
            logger.warn("Attention!!! You tried to delete a node from an already empty list!");
            return null;
        }else if (node == null) {
            logger.warn("You tried to delete 'null' or a value not contained in the corresponding list. Return value is: null.");
            return null;}
        //distinguish between safe-mode and not-safe-mode (test if node is item in this list)
        if ( safeMode ){
            //check if node belongs to this list
            if (node.head == this.first){
                if ( this.length == 1){
                    this.first = null;
                    this.length = 0;
                }else{
                    //if the deleted node was the first one, head needs to be updated for all other nodes
                    if ( node == this.first ){
                        makeHead(node.next);
                    }
                    node.next.previous = node.previous;
                    node.previous.next = node.next;
                    this.length--;
                }
            }else{
                logger.warn("You tried to delete a node which didn't belong to the list! Return value is: null.");
                return null;
            }
            return node;
        }else{
            if (node == this.first && this.length == 1){
                this.first = null;
                this.length = 0;
            }else if ( node == this.first){
                first = first.next;
            }
            node.next.previous = node.previous;
            node.previous.next = node.next;
            this.length--;
            return node;
        }
    }

    /**
     * Removes the last node from the list and returns it.
     * @return the last node from the list while removing it from the list
     */
    public Node<T> pop(){
        return delete(first.previous);
    }

    /**
     * Returns first node whith node.getValue().equals(value).
     * @param value value to search for
     * @return first node with searched value, null if no match was found or value was null or if list was empty
     */
    Node<T> findNodeByValue(T value){

        Node<T> current = first;

        if ( value == null ){
            logger.warn("You tried to find null in a list!");
            return null;
        }

        if ( current == null){
            logger.warn("You tried to find something in an empty list! Return value is null!");
            return null;
        }

        do{
            if (current.getValue().equals(value)){
                return current;
            }else {
                current = current.next;
            }
        }while( current != first );

        return null;
    }

    /**
     * Returns the "first" node of the circular list.
     * @return first node
     */
    public Node<T> getFirst(){
        return this.first;
    }

    /**
     * Tests if the given node's head reference is the same as the one for the list
     * that this method is performed on and if yes updates the list to have the given node
     * as its new head.
     * @param newHead the node which should be the new head of the list
     */
    public void makeHead(Node<T> newHead){
        if( isEmpty() ){
            logger.error("You tried to make a node head of an empty list!");
            System.exit(1);
        }

        if (safeMode){
            if (newHead.getHead() == this.first){
                this.first = newHead;
                newHead.head = newHead;
                Node<T> current = newHead.getNext();
                while (current != newHead){
                    current.head = newHead;
                    current = current.getNext();
                }
            }else{
                logger.error("You updated a node to be head of a list that the node didn't belong to!");
                System.exit(1);
            }
        }else {
            this.first = newHead;
        }

    }

    /**
     * Returns the current number of elements contained in the list.
     * @return number of elements in the list
     */
    public int getLength() {
        return length;
    }

    public boolean isEmpty(){
        return this.length == 0;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        Node<T> current = first;
        stringBuilder.append("List of length ").append(this.length).append(" with elements: \n");
        if (first != null){
            do{
                stringBuilder.append("- ").append(current.toString()).append("\n");
                current = current.next;
            }while ( current != first);
        }else{
            stringBuilder.append("List is empty.");
        }

        return stringBuilder.toString();
    }

    public boolean isInSafeMode() {
        return safeMode;
    }

    public void setSafeMode(boolean safeMode) {
        this.safeMode = safeMode;
    }

    @NotNull
    @Override
    public Iterator iterator() {
        return new CircularListIterator<>(this);
    }

    /**
     * Realizes a concrete Iterator-implementation for the Iterator-pattern for CircularList.
     * Used to iterate over the values of nodes of the containing list.
     * @param <T> generic type parameter derived from the enclosing CircularList's type parameter
     */
    private class CircularListIterator<T> implements Iterator{

        private Node<T> first;
        private Node<T> currentNode;
        private boolean firstVisited;

        public CircularListIterator(CircularList<T> list){
            this.first = list.getFirst();
            currentNode = first;
            firstVisited = false;
        }

        @Override
        public boolean hasNext() {
            if (currentNode == first && !firstVisited){
                return true;
            }else if(currentNode == first && firstVisited){
                return false;
            }else{
                return true;
            }
        }

        @Override
        public T next() {
            if (this.hasNext()){
                if (currentNode == first && !firstVisited){
                    firstVisited = true;
                    currentNode = currentNode.getNext();
                    return first.getValue();
                }else {
                    T element = currentNode.getValue();
                    currentNode = currentNode.getNext();
                    return element;
                }
            }else{
                return null;
            }
        }
    }

    /**
     * Models a generic node of a circular list. Note that it is NOT secured that such a node cannot be instantiated
     * without a corresponding list, due to its designed use-case which makes it necessary to declare it as a static
     * inner class. Nevertheless this should be avoided!
     * @param <U> type of the values to be stored in a node
     */
    public static class Node<U>{

        /**
         * Reference to this node's previous node.
         */
        private Node<U> previous;
        /**
         * Reference to this node's next node.
         */
        private Node<U> next;
        /**
         * The value that is saved inside this node.
         */
        private U value;
        //reference to first node of same list to fasten some operations
        /**
         * Reference to the first list element of the list that this node belongs to.
         * It exists to fasten up certain delete operations that are done with a node as input
         * rather than with a value, reducing the complexity to O(1) for most of these operations.
         * It is for example used to perform a fast check if the node in question belongs to the list.
         */
        private Node<U> head;

        /**
         * Generates a node with the given value and head = null.
         * @param value the value the node will hold after creation
         */
        private Node(U value){
            this.value = value;
            this.head = null;
        }

        /**
         * Returns the node's value.
         * @return the node's value
         */
        public U getValue() {
            return value;
        }

        /**
         * Returns the current node's successor.
         * @return the current node's successor
         */
        public Node<U> getNext(){
            return this.next;
        }

        /**
         * Returns the first element of the list that the current node belongs to.
         * @return first element of the list that the current node belongs to.
         */
        public Node<U> getHead(){
            return this.head;
        }

        /**
         * Returns the current node's predecessor.
         * @return the current node's predecessor
         */
        public Node<U> getPrevious(){
            return this.previous;
        }

        @Override
        public String toString() {
            return this.value.toString();
        }

    }
}
