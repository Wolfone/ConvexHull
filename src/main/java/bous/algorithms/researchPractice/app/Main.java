package bous.algorithms.researchPractice.app;

import bous.algorithms.researchPractice.geometry.ConvexHullCalculator;
import bous.algorithms.researchPractice.geometry.structures.Vertex;
import bous.algorithms.researchPractice.utility.datastructures.CircularList;
import bous.algorithms.researchPractice.utility.geometry.GeometryFactory;
import bous.algorithms.researchPractice.utility.geometry.GeometryWriter;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import bous.algorithms.researchPractice.utility.geometry.GeometryReader;

import java.io.File;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Provides CLI-Functions for the Project.
 */
public class Main {

    private static Logger logger = LogManager.getLogger(GeometryReader.class);

    /**
     * For explanations concerning program-usage see the repo-Readme linked in the method-details-section.
     * @param args Possible input and flags are described in the Repo-Readme.
     * @see <a href="https://gitlab.com/Wolfone/ConvexHull">Source-Code-Repository</a>
     */
    public static void main(String[] args) {

        ConvexHullCalculator convexHullCalculator = null;

        boolean verboseLogging = false;
        boolean safeMode = false;

        String path = args[0];

        if (Arrays.asList(args).contains("-v") || Arrays.asList(args).contains("--verboseLogging")){
            verboseLogging = true;
        }

        if (Arrays.asList(args).contains("-s") || Arrays.asList(args).contains("--safeMode")){
            safeMode = true;
        }

        if (Arrays.asList(args).contains("-c") || Arrays.asList(args).contains("--configurationFile")){
            if (verboseLogging || safeMode){
                logger.warn("You already specified a flag for verbose logging or safe-mode -> config file will not be used.");
            }else{
                try{
                    Configurations configs = new Configurations();
                    Configuration config = configs.properties(new File("configuration.properties"));

                    verboseLogging = config.getBoolean("verboseLogging");
                    safeMode = config.getBoolean("safeMode");
                }catch (ConfigurationException e){
                    e.printStackTrace();
                    logger.warn("Something went wrong during configuration process!");
                }
            }
        }

        if (Arrays.asList(args).contains("-r") || Arrays.asList(args).contains("--randomized")){
            Scanner scanner = new Scanner(System.in);
            System.out.println("You chose to generate vertices randomly. If you specified an input file, it will be ignored.");
            System.out.println("Please enter an integer value to represent the maximum value a vertex-coordinate should have: ");
            int maxValue = scanner.nextInt();
            System.out.println("Please enter an integer value to represent the total number of vertices to be generated: ");
            int totalNumber = scanner.nextInt();
            convexHullCalculator = constructHullFromRandomVertices(maxValue, totalNumber, verboseLogging, safeMode);
        }else if (path.equals("") || path.startsWith("-")){
            logger.warn("No input file specified!");
            System.exit(0);
        }else {
            convexHullCalculator = constructHullFromCSVInputFile(path, verboseLogging, safeMode);
        }

        if (Arrays.asList(args).contains("-e") || Arrays.asList(args).contains("--export")){
            System.out.println("You asked to generate an export of the generated hull-description; please specify a file-name/path (without filetype-suffix): ");
            Scanner scanner = new Scanner(System.in);
            String objFilePath = scanner.next();
            GeometryWriter.exportPolytopeToObjFile(objFilePath + ".obj", convexHullCalculator.getPolytope());
        }
    }

    /**
     * Reads the contents of a CSV-File as understood by GeometryReader.readVerticesFromCSV and constructs the Hull from the input.
     * @param pathToFile Path where the input file resides.
     * @param verboseLogging Turns verbose logging on or off.
     * @param safeMode Specifies if the used circular lists should operate in safe mode; debugging purposes only.
     * @return An instance of ConvexHullCalculator with its datastructure already representing the calculated hull.
     */
    private static ConvexHullCalculator constructHullFromCSVInputFile(String pathToFile, boolean verboseLogging, boolean safeMode){
        ConvexHullCalculator convexHullCalculator = new ConvexHullCalculator(verboseLogging, safeMode);
        CircularList<Vertex> vertices = GeometryReader.readVerticesFromCSV(new File(pathToFile), verboseLogging, safeMode);
        logger.info("The read input is as follows:");
        System.out.println(vertices);
        convexHullCalculator.setVertices(vertices);
        convexHullCalculator.createDoubleTriangleAndConstructHull();
        logger.info("The resulting list of vertices describing the convex hull is as follows: ");
        System.out.println(convexHullCalculator.getVertices());
        if (verboseLogging){
            logger.info("The resulting list of faces describing the convex hull is as follows: ");
            System.out.println(convexHullCalculator.getFaces());
        }
        return convexHullCalculator;
    }

    /**
     * Similar to constructHullFromCSVInputFile but uses random values.
     * @param maxValue Maximum integer value that a coordinate of a vertex may have.
     * @param totalNumber Number of nodes that should be created as input.
     * @param verboseLogging Turns verbose logging on or off.
     * @param safeMode Specifies if the used circular lists should operate in safe mode; debugging purposes only.
     * @return An instance of ConvexHullCalculator with its datastructure already representing the calculated hull.
     */
    private static ConvexHullCalculator constructHullFromRandomVertices(int maxValue, int totalNumber, boolean verboseLogging, boolean safeMode){
        ConvexHullCalculator convexHullCalculator = new ConvexHullCalculator(verboseLogging, safeMode);
        CircularList<Vertex> vertices = GeometryFactory.randomizedVertexList(maxValue, totalNumber);
        logger.info("The generated input is as follows:");
        System.out.println(vertices);
        convexHullCalculator.setVertices(vertices);
        convexHullCalculator.createDoubleTriangleAndConstructHull();
        logger.info("The resulting list of vertices describing the convex hull is as follows: ");
        System.out.println(convexHullCalculator.getVertices());
        if (verboseLogging){
            logger.info("The resulting list of faces describing the convex hull is as follows: ");
            System.out.println(convexHullCalculator.getFaces());
        }
        return convexHullCalculator;
    }

}
