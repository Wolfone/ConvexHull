package bous.algorithms.researchPractice.geometry.structures;

import java.util.Arrays;

/**
 * Represents a vertex in Z^3.
 */
public class Vertex {

    /**
     * Coordinates of the vertex.
     */
    private int[] vertexCoordinates;
    /**
     * Identifier of the vertex.
     */
    private int id;
    /**
     * Reference to incident cone edge.
     */
    private Edge duplicate;

    // Flags
    /**
     * Helper-flag to signal that the vertex is on the hull.
     */
    private boolean onHull;
    /**
     * Helper-flag to signal that the vertex was already processed.
     */
    private boolean processed;

    /**
     * Creates an "empty" vertex.
     */
    public Vertex(){
        this.vertexCoordinates = new int[]{0, 0, 0};
        this.id = 0;
        this.duplicate = null;
        this.onHull = false;
        this.processed = false;
    }

    /**
     * Creates a vertex based on 3 input-coordinates.
     * @param x The x coordinate of the newly created vertex.
     * @param y The y coordinate of the newly created vertex.
     * @param z The z coordinate of the newly created vertex.
     */
    public Vertex(int x, int y, int z){
        this();
        int[] coordinates = new int[]{x, y, z};
        this.setVertexCoordinates(coordinates);
    }

    /**
     * Creates a vertex based on 3 input-coordinates and sets its ID.
     * @param x The x coordinate of the newly created vertex.
     * @param y The y coordinate of the newly created vertex.
     * @param z The z coordinate of the newly created vertex.
     * @param id The id of the newly created vertex.
     */
    public Vertex(int x, int y, int z, int id){
        this(x,y,z);
        this.setId(id);
    }

    /**
     * Creates a vertex from coordinates given as an int-array.
     * @param coordinates The coordinates as array.
     */
    public Vertex(int[] coordinates){
        this();
        if(coordinates.length != 3){
            throw new IllegalArgumentException("Can't create Vertex from int array of length != 3");
        }
        this.setVertexCoordinates(coordinates);
    }

    /**
     * Tests the vertex for equality of the coordinates with those of a given second vertex.
     * Doesn't test for same ID as equals(...) does!
     * @param vertex the vertex to compare to.
     * @return true if coordinates are the same, false if not.
     */
    public boolean hasSameCoordinates(Vertex vertex){
        return (this.getX() == vertex.getX()) && (this.getY() == vertex.getY()) && (this.getZ() == vertex.getZ());
    }

    @Override
    public String toString() {
        return "(Vertex with ID: "
                + id
                + " , coordinates: ("
                + vertexCoordinates[0] + ", "
                + vertexCoordinates[1] + ", "
                + vertexCoordinates[2] + "))";
    }

    @Override
    public boolean equals(Object obj) {
        Vertex vertex = (Vertex) obj;
        if (Arrays.equals(this.vertexCoordinates, vertex.vertexCoordinates) && this.id == vertex.id){
            return true;
        }else{
            return false;
        }
    }

    //<editor-fold desc="Getters and Setters">
    public int getX(){
        return vertexCoordinates[0];
    }

    public int getY(){
        return vertexCoordinates[1];
    }

    public int getZ(){
        return vertexCoordinates[2];
    }

    public int[] getVertexCoordinates() {
        return vertexCoordinates;
    }

    public void setVertexCoordinates(int[] vertexCoordinates) {
        this.vertexCoordinates = vertexCoordinates;
    }

    public void setVertexCoordinates(int x, int y, int z){
        this.vertexCoordinates = new int[]{x, y, z};
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isOnHull() {
        return onHull;
    }

    public void setOnHull(boolean onHull) {
        this.onHull = onHull;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public Edge getDuplicate() {
        return duplicate;
    }

    public void setDuplicate(Edge duplicate) {
        this.duplicate = duplicate;
    }
    //</editor-fold>
}
