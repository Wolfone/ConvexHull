package bous.algorithms.researchPractice.geometry.structures;

/**
 * Represents a triangular face.
 */
public class Face {

    /**
     * The defining edges of the face (counterclockwise as perceived from the "upper" side of the edge).
     */
    private Edge[] definingEdges;
    /**
     * The defining vertices of the face (counterclockwise as perceived from the "upper" side of the edge).
     */
    private Vertex[] definingVertices;
    /**
     * Helper to indicate if a face is visible from a certain vertex.
     */
    private boolean visible;
    /**
     * Identifier of the face.
     */
    private int id;

    /**
     * Creates an instance with empty but initialized arrays for the respective fields and the visibility of the
     * face set to false by default;
     */
    public Face(){
        this.definingEdges = new Edge[3];
        this.definingVertices = new Vertex[3];
        this.visible = false;
    }

    /**
     * Constructs a basic face-object from three given vertices, remark, that the orientation of the face
     * depends on the order in which the vertices are passed into the constructor. The resulting edges will be
     * constructed as follows: e0 = (v0,v1), e1 = (v1,v2) , e2 = (v2,v0)
     * @param v0 the first vertex
     * @param v1 the second vertex
     * @param v2 the third vertex
     */
    public Face(Vertex v0, Vertex v1, Vertex v2){
        this();
        definingVertices[0] = v0;
        definingVertices[1] = v1;
        definingVertices[2] = v2;

        definingEdges[0] = new Edge(v0, v1);
        definingEdges[1] = new Edge(v1, v2);
        definingEdges[2] = new Edge(v2, v0);
    }

    //<editor-fold desc="Getters and Setters">
    public Edge[] getDefiningEdges() {
        return definingEdges;
    }

    public void setDefiningEdges(Edge[] definingEdges) {
        this.definingEdges = definingEdges;

    }public void setDefiningEdges(Edge e0, Edge e1, Edge e2) {
        this.definingEdges = new Edge[]{e0, e1, e2};
    }

    public Vertex[] getDefiningVertices() {
        return definingVertices;
    }

    public void setDefiningVertices(Vertex[] definingVertices) {
        this.definingVertices = definingVertices;
    }

    public void setDefiningVertices(Vertex v0, Vertex v1, Vertex v2) {
        this.definingVertices = new Vertex[]{v0, v1, v2};
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //</editor-fold>


    @Override
    public String toString() {
        return ("Face with ID " + id + ": \n") + "\nDefining vertices:\n" +
                definingVertices[0].toString() + "\n" +
                definingVertices[1].toString() + "\n" +
                definingVertices[2].toString() + "\n" +
                "\nDefining edges:\n" +
                definingEdges[0].toString() + "\n" +
                definingEdges[1].toString() + "\n" +
                definingEdges[2].toString() + "\n\n";
    }
}
