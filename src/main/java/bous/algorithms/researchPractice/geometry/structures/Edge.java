package bous.algorithms.researchPractice.geometry.structures;

/**
 * Represents a geometrical edge.
 */
public class Edge {

    /**
     * The faces adjacent to this edge.
     */
    private Face[] adjacentFaces;
    /**
     * The vertices that define this edges endpoints.
     */
    private Vertex[] endPoints;
    /**
     * A helper reference that may temporarily be used rearrange the edge-structure of a convex hull.
     */
    private Face newFace;
    /**
     * Signals if the edge should be deleted (true if yes).
     */
    private boolean toBeDeleted;
    /**
     * Identifier of the edge.
     */
    private int id;

    /**
     * Creates an "empty" edge.
     */
    public Edge(){
        this.adjacentFaces = new Face[2];
        this.endPoints = new Vertex[2];
        this.newFace = null;
        this.toBeDeleted = false;
    }

    /**
     * Creates an edge from 2 given endpoints.
     * @param v0 Endpoint 1.
     * @param v1 Endpoint 2.
     */
    public Edge(Vertex v0, Vertex v1){
        this();
        endPoints[0] = v0;
        endPoints[1] = v1;
    }

    @Override
    public String toString() {
        return ("Edge with ID " + id + " and ") +
                "Start-Point: " + endPoints[0] +
                "End-Point: " + endPoints[1] +
                " with adjacent faces (IDs): " + this.adjacentFaces[0].getId() + " and " + this.adjacentFaces[1].getId();
    }

    //<editor-fold desc="Getters and Setters">
    public Face[] getAdjacentFaces() {
        return adjacentFaces;
    }

    public void setAdjacentFaces(Face[] adjacentFaces) {
        this.adjacentFaces = adjacentFaces;
    }

    public void setAdjacentFace(int index, Face face){
        this.adjacentFaces[index] = face;
    }

    public void setAdjacentFaces(Face f0, Face f1) {
        this.adjacentFaces = new Face[]{f0, f1};
    }

    public Vertex[] getEndPoints() {
        return endPoints;
    }

    public void setEndPoints(Vertex[] endPoints) {
        this.endPoints = endPoints;
    }

    public void setEndPoints(Vertex startPoint, Vertex endPoint) {
        Vertex[] newEndpoints = new Vertex[2];
        newEndpoints[0] = startPoint;
        newEndpoints[1] = endPoint;
        this.endPoints = newEndpoints;
    }

    public Face getNewFace() {
        return newFace;
    }

    public void setNewFace(Face newFace) {
        this.newFace = newFace;
    }

    public boolean hasToBeDeleted() {
        return toBeDeleted;
    }

    public void setToBeDeleted(boolean toBeDeleted) {
        this.toBeDeleted = toBeDeleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    //</editor-fold>
}
