package bous.algorithms.researchPractice.geometry.structures;

import bous.algorithms.researchPractice.utility.datastructures.CircularList;

import java.util.Iterator;

/**
 * Represents a polytope made up by circular lists of vertices, edges and faces-
 */
public class Polytope {

    /**
     * The defining vertices.
     */
    private CircularList<Vertex> vertices;
    /**
     * The defining edges.
     */
    private CircularList<Edge> edges;
    /**
     * The defining faces.
     */
    private CircularList<Face> faces;

    /**
     * Creates an "empty" polytope.
     */
    public Polytope(){
        vertices= new CircularList<>();
        edges= new CircularList<>();
        faces = new CircularList<>();
    }

    /**
     * Reindexes the polytope so its vertices,edges and faces are numbered from 1 upwards to their respective max-number.
     * This is especially useful as a first step to export a polytope in *.obj-format, which is the primary intended use-case.
     */
    public void reindexPolytope(){
        int counter = 1;

        Iterator<Vertex> vertexIterator = vertices.iterator();
        while (vertexIterator.hasNext()) {
            Vertex currentVertex =  vertexIterator.next();
            currentVertex.setId(counter++);
        }

        counter = 1;

        Iterator<Edge> edgeIterator = edges.iterator();
        while (edgeIterator.hasNext()) {
            Edge currentEdge = edgeIterator.next();
            currentEdge.setId(counter);
        }

        counter = 1;

        Iterator<Face> faceIterator = faces.iterator();
        while (faceIterator.hasNext()) {
            Face currentFace = faceIterator.next();
            currentFace.setId(counter);
        }
    }

    //<editor-fold desc="Getters and Setters">
    public CircularList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(CircularList<Edge> edges) {
        this.edges = edges;
    }

    public CircularList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(CircularList<Vertex> vertices) {
        this.vertices = vertices;
    }

    public CircularList<Face> getFaces() {
        return faces;
    }

    public void setFaces(CircularList<Face> faces) {
        this.faces = faces;
    }

    //</editor-fold>
}
