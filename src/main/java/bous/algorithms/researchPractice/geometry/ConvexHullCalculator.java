package bous.algorithms.researchPractice.geometry;

import bous.algorithms.researchPractice.geometry.structures.Edge;
import bous.algorithms.researchPractice.geometry.structures.Face;
import bous.algorithms.researchPractice.geometry.structures.Polytope;
import bous.algorithms.researchPractice.geometry.structures.Vertex;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import bous.algorithms.researchPractice.utility.datastructures.CircularList;
import bous.algorithms.researchPractice.utility.datastructures.CircularList.Node;
import bous.algorithms.researchPractice.utility.geometry.GeometryReader;

import static bous.algorithms.researchPractice.utility.geometry.GeometryPropertyChecks.calculateVolumeSign;
import static bous.algorithms.researchPractice.utility.geometry.GeometryPropertyChecks.checkVertexCollinearity;

/**
 * Core class that wraps functions and datastructures to calculate the convex hull of a given set of vertices.
 */
public class ConvexHullCalculator {

    private static Logger logger = LogManager.getLogger(GeometryReader.class);
    /**
     * Determines if verbose logging is performed.
     */
    private boolean verboseLogging;
    /**
     * Counts the number of faces.
     */
    private int faceCounter = 0;
    /**
     * Counts the number of edges.
     */
    private int edgeCounter = 0;

    private CircularList<Vertex> vertices;
    private CircularList<Edge> edges;
    private CircularList<Face> faces;

    /**
     * Creates an instance with the option to specify if it runs in safe-mode.
     * @param safeMode True triggers some coherence checks for lists.
     */
    public ConvexHullCalculator(boolean safeMode){
        vertices= new CircularList<>(safeMode);
        edges= new CircularList<>(safeMode);
        faces = new CircularList<>(safeMode);
    }

    /**
     * Creates an instance with the option to specify if verbose logging is turned on and if it runs in safe-mode.
     * @param verboseLogging True triggers verbose logging.
     * @param safeMode True triggers some coherence checks for lists.
     */
    public ConvexHullCalculator(boolean verboseLogging, boolean safeMode){
        this.verboseLogging = verboseLogging;
        vertices= new CircularList<>(safeMode);
        edges= new CircularList<>(safeMode);
        faces = new CircularList<>(safeMode);
    }

    /**
     * Returns the polytope described by the instances vertices, faces and edges as an instance of the Polytope-class.
     * @return Instance of the Polytope-class
     */
    public Polytope getPolytope(){
        Polytope polytope = new Polytope();
        polytope.setVertices(this.vertices);
        polytope.setEdges(this.edges);
        polytope.setFaces(this.faces);

        return polytope;
    }

    /**
     * Constructs the hull of the given set of vertices. Beware: should only be used after a call of createDoubleTriangle.
     */
    private void constructHull(){
        Node<Vertex> vertexNode;
        Node<Vertex> vertexNodeNext;
        Vertex currentVertex;

        vertexNode = vertices.getFirst();

        currentVertex = vertexNode.getValue();
        do {
            vertexNodeNext = vertexNode.getNext();
            if (!currentVertex.isProcessed()){
                currentVertex.setProcessed(true);
                processVertex(currentVertex);
                cleanUp();
            }
            vertexNode = vertexNodeNext;
            currentVertex = vertexNode.getValue();
        }while (vertexNode != vertices.getFirst());
    }

    /**
     * Creates the initial double-triangle (if the given vertices allow this) which is used as a base to begin
     * further calculations with and sets the vertice-list's head to a vertex that is non-coplanar with the vertices
     * that make up the initial triangles; this is a necessary step before the execution of constructHull and therefore
     * encapsulated in createDoubleTriangleAndConstructHull.
     */
    private void createDoubleTriangle(){
        //check if there are vertices to calculate something on
        if (this.vertices == null || this.vertices.isEmpty() || this.vertices.getLength() < 4){
            logger.warn("You tried to calculate a convex hull for a set of vertices with less than 4 elements.");
            return;
        }

        //get first vertex from vertex-list
        Node<Vertex> firstVertex = vertices.getFirst();
        Node<Vertex> v0 = firstVertex;
        Node<Vertex> v1, v2, v3, t;
        Face f0, f1 = null;
        Edge e0, e1, e2, s;
        int volumeSign;

        /* First: search for 3 non-collinear points to start from; collinearity would result in a zero-area and
           therefore lead to zero-volume tetrahedra. To do that loop through the vertices and in each iteration
           check subsequent three vertices for collinearity, if they are not collinear don't repeat the loop and
           set v0-v2 to the found non-collinear vertices. If we reach the "beginning" of the circular list again
           without success in finding such three vertices, we know that all of them must be collinear and we terminate.
         */
        while (checkVertexCollinearity(v0.getValue(), v0.getNext().getValue(), v0.getNext().getNext().getValue())){
            if ( (v0 = v0.getNext()) == firstVertex ){
                logger.info("All given points were collinear!");
                System.exit(0);
            }
        }

        //set references to the found vertice-nodes, and mark them as processed
        v1 = v0.getNext();
        v2 = v1.getNext();
        v0.getValue().setProcessed(true);
        v1.getValue().setProcessed(true);
        v2.getValue().setProcessed(true);

        //create the two initial twin-faces
        f0 = constructInitialFace(v0.getValue(), v1.getValue(), v2.getValue(), f1);
        f1 = constructInitialFace(v2.getValue(), v1.getValue(), v0.getValue(), f0);

        //link adjacent face fields
        f0.getDefiningEdges()[0].getAdjacentFaces()[1] = f1;
        f0.getDefiningEdges()[1].getAdjacentFaces()[1] = f1;
        f0.getDefiningEdges()[2].getAdjacentFaces()[1] = f1;
        f1.getDefiningEdges()[0].getAdjacentFaces()[1] = f0;
        f1.getDefiningEdges()[1].getAdjacentFaces()[1] = f0;
        f1.getDefiningEdges()[2].getAdjacentFaces()[1] = f0;

        //find fourth, non-coplanar point to form a tetrahedron
        v3 = v2.getNext();
        volumeSign = calculateVolumeSign(f0, v3.getValue());
        while ( volumeSign == 0){
            if ((v3 = v3.getNext()) == v0){
                logger.info("createDoubleTriangle: all given points are coplanar");
                System.exit(0);
            }
            volumeSign = calculateVolumeSign(f0, v3.getValue());
        }

        vertices.makeHead(v3);
    }

    /**
     * Wraps "createDoubleTriangle" and "constructHull" into a single function for these two
     * functions usually are executed together.
     */
    public void createDoubleTriangleAndConstructHull(){
        if (this.vertices.isEmpty() || this.vertices == null){
            logger.warn("No vertices set or vertices-list is empty.");
            System.exit(0);
        }else{
            createDoubleTriangle();
            constructHull();
        }
    }

    /**
     * This function's only purpose is to help construct the first two initial faces; if one fold is null, a new
     * face will be build from scratch (that's what happens to the first face). The second face will have the
     * first available as a non-null fold-input, so the already constructed edges can be reused with inverted ordering.
     * @param v0 first input vertex
     * @param v1 second input vertex
     * @param v2 third input vertex
     * @param fold the "twin"-face/null if the twin face is not already constructed
     */
    private Face constructInitialFace(Vertex v0, Vertex v1, Vertex v2, Face fold){
        Face face;
        Edge e0, e1, e2;

        //case: this is the first of the initial twin-faces -> edges need to be newly constructed
        if (fold == null){
            e0 = makeNullEdge();
            e1 = makeNullEdge();
            e2 = makeNullEdge();
        //case: this is the second of the initial twin-faces -> edges will be reused
        } else {
            e0 = fold.getDefiningEdges()[2];
            e1 = fold.getDefiningEdges()[1];
            e2 = fold.getDefiningEdges()[0];
        }
        //set endpoints of edges
        e0.setEndPoints(v0, v1);
        e1.setEndPoints(v1, v2);
        e2.setEndPoints(v2, v0);

        //construct the new face with the edges and given vertices
        face = makeNullFace();
        face.setDefiningEdges(e0, e1, e2);
        face.setDefiningVertices(v0, v1, v2);

        //link the newly created edges to the new face; at the moment there is only one adjacent face
        e0.setAdjacentFaces(face, null);
        e1.setAdjacentFaces(face, null);
        e2.setAdjacentFaces(face, null);

        return face;
    }

    /**
     * Encapsulates the actions necessary to process the next vertex during the calculation of the convex hull.
     * This especially includes checking if the vertex is outside the already calculated hull or not as well as
     * triggering the construction of a new faces if necessary.
     * @param vertex The vertex to be processed.
     * @return Returns false if the given vertex is already inside the hull and true else.
     */
    private boolean processVertex(Vertex vertex){
        Node<Face> faceNode;
        Node<Edge> edgeNode;
        Node<Edge> tempNode;
        //helper to improve readability of second do-while-loop
        Edge currentEdge;
        boolean visible = false;

        //mark faces visible from p, this is determined by the volume-sign of the corresponding tetrahedron
        faceNode = faces.getFirst();
        do {
            if (calculateVolumeSign(faceNode.getValue(), vertex) < 0){
                faceNode.getValue().setVisible(true);
                visible = true;
            }
            faceNode = faceNode.getNext();
        }while(faceNode != faces.getFirst());

        //if no faces are visible from p, then p is inside the hull
        if (!visible){
            vertex.setOnHull(false);
            return false;
        }

        //mark edges in interior of visible region for deletion
        //create newFaces with border-edges as their bases
        edgeNode = edges.getFirst();
        do {
            tempNode = edgeNode.getNext();
            currentEdge = edgeNode.getValue();
            //if both adjacent faces of the currentEdge are visible, we conclude: currentEdge interior -> mark for deletion
            if (currentEdge.getAdjacentFaces()[0].isVisible() && currentEdge.getAdjacentFaces()[1].isVisible()){
                currentEdge.setToBeDeleted(true);
            //if only one of the adjacent faces is visible we can conclude: currentEdge is on border -> make a new face
            }else if (currentEdge.getAdjacentFaces()[0].isVisible() || currentEdge.getAdjacentFaces()[1].isVisible()){
                currentEdge.setNewFace(makeConeFace(currentEdge, vertex));
            }
            edgeNode = tempNode;
        }while (edgeNode != edges.getFirst());

        return true;
    }

    /**
     * Wraps the necessary steps to reestablish correctness of the used datastructures after one vertex was processed.
     */
    private void cleanUp(){
        cleanEdges();
        cleanFaces();
        cleanVertices();
    }

    /**
     * Removes all faces that are marked for deletion.
     */
    private void cleanFaces(){
        //primary pointer into face-list
        Node<Face> faceNode;
        //temporary pointer for deleting
        Node<Face> tempFaceNode;

        faceNode = faces.getFirst();
        //this will be slow if the list is in safe-mode; use safe-mode only for debugging
        //delete all faces marked as visible
        while(!faces.isEmpty() && faces.getFirst().getValue().isVisible()){
            faceNode = faces.getFirst();
            faces.delete(faceNode);
        }

        faceNode = faces.getFirst().getNext();
        do {
            if (faceNode.getValue().isVisible()){
                tempFaceNode = faceNode;
                faceNode = faceNode.getNext();
                faces.delete(tempFaceNode);
            }else {
                faceNode = faceNode.getNext();
            }
        } while (faceNode != faces.getFirst());
    }

    /**
     * Performs all necessary steps to reestablish correct edge-structure after processing of a vertex.
     * This especially contains steps to set the adjacent faces of border-edges to reference the correct faces.
     */
    private void cleanEdges(){
        Node<Edge> edgeNode;
        Node<Edge> tempEdgeNode;

        Edge currentEdge;
        //iterate over all edges and integrate the newFaces into the data-structure if set
        edgeNode = edges.getFirst();
        do {
            currentEdge = edgeNode.getValue();
            if (currentEdge.getNewFace() != null){
                if (currentEdge.getAdjacentFaces()[0].isVisible()){
                    currentEdge.setAdjacentFace(0,currentEdge.getNewFace());
                }else{
                    currentEdge.setAdjacentFace(1, currentEdge.getNewFace());
                }
                currentEdge.setNewFace(null);
            }
            edgeNode = edgeNode.getNext();
        }while(edgeNode != edges.getFirst());

        //delete any edges marked for deletion
        while (!edges.isEmpty() && edges.getFirst().getValue().hasToBeDeleted()){
            edgeNode = edges.getFirst();
            edges.delete(edgeNode);
            //System.out.println("Predeleted: " + edgeNode);
        }
        edgeNode = edges.getFirst().getNext();
        do {
            if (edgeNode.getValue().hasToBeDeleted()){
                tempEdgeNode = edgeNode;
                edgeNode = edgeNode.getNext();
                edges.delete(tempEdgeNode);
            }else{
                edgeNode = edgeNode.getNext();
            }
        } while (edgeNode != edges.getFirst());
    }

    /**
     * Performs all necessary steps to reestablish correct vertex-structure after processing of a vertex.
     * This means: finding out which vertices may be deleted as well as the deletion itself
     * and reset of relevant vertex-flags.
     */
    private void cleanVertices(){
        Node<Edge> edgeNode;
        Node<Vertex> vertexNode;
        Node<Vertex> tempVertexNode;
        Vertex currentVertex;

        //mark all vertices incident to some undeleted edges as on the hull
        edgeNode = edges.getFirst();
        do {
            edgeNode.getValue().getEndPoints()[0].setOnHull(true);
            edgeNode.getValue().getEndPoints()[1].setOnHull(true);
            edgeNode = edgeNode.getNext();
        } while (edgeNode != edges.getFirst());

        //delete all vertices that have been processed but are not on the hull
        while (!vertices.isEmpty() && vertices.getFirst().getValue().isProcessed() && !vertices.getFirst().getValue().isOnHull()){
            vertexNode = vertices.getFirst();
            if (verboseLogging){
                logger.info("Remove vertex: " + vertexNode);
            }
            vertices.delete(vertexNode);
        }
        vertexNode = vertices.getFirst().getNext();
        do {
            currentVertex = vertexNode.getValue();
            if (currentVertex.isProcessed() && !currentVertex.isOnHull()){
                tempVertexNode = vertexNode;
                vertexNode = vertexNode.getNext();
                if (verboseLogging){
                    logger.info("Remove vertex: " + tempVertexNode);
                }
                vertices.delete(tempVertexNode);
            }else {
                vertexNode = vertexNode.getNext();
            }
        } while(vertexNode != vertices.getFirst());

        //reset flags
        vertexNode = vertices.getFirst();
        do {
            currentVertex = vertexNode.getValue();
            currentVertex.setDuplicate(null);
            currentVertex.setOnHull(false);
            vertexNode = vertexNode.getNext();
        }while (vertexNode != vertices.getFirst());
    }

    /**
     * For a given edge and a given vertex, this method constructs a face that is spanned by this edge and vertex
     * (including construction of the additionally needed edges between the vertex and the edge's endpoints).
     * @param edge the edge
     * @param vertex the vertex
     * @return the newly created face
     */
    private Face makeConeFace(Edge edge, Vertex vertex){
        Edge[] newEdges = new Edge[2];
        Face newFace;
        int i, j;

        //make two new edges if they don't already exist
        for (i = 0; i < 2; ++i){
            //copy the edge into newEdges; remark: it may be non-existent
            newEdges[i] = edge.getEndPoints()[i].getDuplicate();
            //if it does not exist (duplicate is null) create a new one
            if (newEdges[i] == null){
                newEdges[i] = makeNullEdge();
                newEdges[i].setEndPoints(edge.getEndPoints()[i], vertex);
                edge.getEndPoints()[i].setDuplicate(newEdges[i]);
            }
        }

        //make new face
        newFace = makeNullFace();
        newFace.setDefiningEdges(edge, newEdges[0], newEdges[1]);
        makeCounterclockwise(newFace, edge, vertex);

        //set adjacent face pointers
        for (i = 0; i < 2; ++i){
            for (j = 0; j < 2; ++j){
                //only one null-link should be set to newFace
                if (newEdges[i].getAdjacentFaces()[j] == null){
                    newEdges[i].getAdjacentFaces()[j] = newFace;
                    break;
                }
            }
        }

        return newFace;
    }

    /**
     * Changes the direction of a given face to be counterclockwise. The signature of this method may seem a little bit
     * odd; this results from the special situation for which it was made. It is supposedly only used inside the method
     * makeConeFace where the input derives from.
     * @param face The face of which the orientation should be set.
     * @param edge The base-edge of the said face as given to makeConeFace.
     * @param vertex The apex-vertex of the face newly created by makeConeFace as given to makeConeFace.
     */
    public void makeCounterclockwise(Face face, Edge edge, Vertex vertex){
        //visible face adjacent to edge
        Face visibleFace;
        //index of edge.endPoints[0] in visibleFace
        int index;
        //helper for swapping
        Edge swapHelper;

        if(edge.getAdjacentFaces()[0].isVisible()){
            visibleFace = edge.getAdjacentFaces()[0];
        }else{
            visibleFace = edge.getAdjacentFaces()[1];
        }

        for(index = 0; visibleFace.getDefiningVertices()[index] != edge.getEndPoints()[0]; ++index);

        //orient f the same as visibleFace
        if (visibleFace.getDefiningVertices()[(index + 1) % 3] != edge.getEndPoints()[1]){
            face.getDefiningVertices()[0] = edge.getEndPoints()[1];
            face.getDefiningVertices()[1] = edge.getEndPoints()[0];
        }else{
            face.getDefiningVertices()[0] = edge.getEndPoints()[0];
            face.getDefiningVertices()[1] = edge.getEndPoints()[1];

            swapHelper = face.getDefiningEdges()[1];
            face.getDefiningEdges()[1] = face.getDefiningEdges()[2];
            face.getDefiningEdges()[2] = swapHelper;
        }

        face.getDefiningVertices()[2] = vertex;
    }

    /**
     * Creates a new vertex without specified values and appends it to the list of vertices
     * @return the newly created vertex
     */
    public Vertex makeNullVertex(){
        Vertex vertex = new Vertex();
        this.vertices.append(vertex);
        return vertex;
    }

    /**
     * Creates a new edge without specified values and appends it to the list of edges
     * @return the newly created edge
     */
    private Edge makeNullEdge(){
        Edge edge = new Edge();
        edge.setId(edgeCounter++);
        this.edges.append(edge);
        return edge;
    }

    /**
     * Creates a new face without specified values and appends it to the list of faces.
     * @return the newly created face
     */
    private Face makeNullFace(){
        Face face = new Face();
        face.setId(faceCounter++);
        this.faces.append(face);
        return face;
    }

    @Override
    public String toString() {
        return String.valueOf(this.vertices) + "\n" +
                this.edges + "\n" +
                this.faces + "\n";
    }

    //<editor-fold desc="Getters and Setters">
    public CircularList<Edge> getEdges() {
        return edges;
    }

    public void setEdges(CircularList<Edge> edges) {
        this.edges = edges;
    }

    public CircularList<Vertex> getVertices() {
        return vertices;
    }

    public void setVertices(CircularList<Vertex> vertices) {
        this.vertices = vertices;
    }

    public CircularList<Face> getFaces() {
        return faces;
    }

    public void setFaces(CircularList<Face> faces) {
        this.faces = faces;
    }

    //</editor-fold>
}
